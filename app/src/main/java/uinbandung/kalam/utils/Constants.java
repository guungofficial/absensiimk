package uinbandung.kalam.utils;

public class Constants {
    public final static String TAG_QR_SCAN = "QR_SCAN";
    public final static int CODE_CAMERA_PERMISSION = 100;
    public final static String DESIRED_SCAN_TYPE = "QR_CODE";

    /**
     * Color matrix that flips the components (<code>-1.0f * c + 255 = 255 - c</code>)
     * and keeps the alpha intact.
     */
    public static final float[] NEGATIVE = {
            -1.0f, 0, 0, 0, 255, // red
            0, -1.0f, 0, 0, 255, // green
            0, 0, -1.0f, 0, 255, // blue
            0, 0, 0, 1.0f, 0  // alpha
    };

    public final static int PILIH_KELAS = 101;

}
