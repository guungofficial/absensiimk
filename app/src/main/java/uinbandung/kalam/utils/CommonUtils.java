package uinbandung.kalam.utils;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.provider.Settings;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import uinbandung.kalam.R;


public class CommonUtils {

    public final static int DAY_MORNING = 0;
    public final static int DAY_MIDDAY = 1;
    public final static int DAY_AFTERNOON = 2;
    public final static int DAY_NIGHT = 3;
    public static ProgressDialog progressDialog;

    public static void dialogTanggal(Context context, final EditText etDate) {
        final DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        TimeZone tz = TimeZone.getTimeZone("Asia/Jakarta");
        dateFormat.setTimeZone(tz);
        Calendar newCalendar = Calendar.getInstance();

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, (view, year, monthOfYear, dayOfMonth) -> {
            Calendar newDate = Calendar.getInstance();
            newDate.set(year, monthOfYear, dayOfMonth);
            etDate.setText(dateFormat.format(newDate.getTime()));
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();

    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void dialogYesNo(Context context, String message, DialogInterface.OnClickListener onPositiveListener, DialogInterface.OnClickListener onNegativeListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setPositiveButton("Ya", onPositiveListener);
        builder.setNegativeButton("Tidak", onNegativeListener);
        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static int getDayType() {
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        if (timeOfDay < 12) {
            return DAY_MORNING;
        } else if (timeOfDay < 15) {
            return DAY_MIDDAY;
        } else if (timeOfDay < 18) {
            return DAY_AFTERNOON;
        } else {
            return DAY_NIGHT;
        }
    }

    public static int getDayResource() {
        int type = getDayType();

        switch (type) {
            case DAY_MORNING:
                return R.drawable.bg_pagi;
            case DAY_MIDDAY:
                return R.drawable.bg_siang;
            case DAY_AFTERNOON:
                return R.drawable.bg_sore;
            case DAY_NIGHT:
                return R.drawable.bg_malam;
        }

        return -1;
    }


    public static void doRestart(Context c) {
        try {
            //check if the context is given
            if (c != null) {
                //fetch the packagemanager so we can get the default launch activity
                // (you can replace this intent with any other activity if you want
                PackageManager pm = c.getPackageManager();
                //check if we got the PackageManager
                if (pm != null) {
                    //create the intent with the default start activity for your application
                    Intent mStartActivity = pm.getLaunchIntentForPackage(
                            c.getPackageName()
                    );
                    if (mStartActivity != null) {
                        mStartActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        //create a pending intent so the application is restarted after System.exit(0) was called.
                        // We use an AlarmManager to call this intent in 100ms
                        int mPendingIntentId = 223344;
                        PendingIntent mPendingIntent = PendingIntent
                                .getActivity(c, mPendingIntentId, mStartActivity,
                                        PendingIntent.FLAG_CANCEL_CURRENT);
                        AlarmManager mgr = (AlarmManager) c.getSystemService(Context.ALARM_SERVICE);
                        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 5, mPendingIntent);
                        //kill the application
                        //System.exit(0);
                        //kill the application
                        //android.os.Process.killProcess(android.os.Process.myPid());
                    } else {
                        Log.e("RAPP", "Was not able to restart application, mStartActivity null");
                    }
                } else {
                    Log.e("RAPP", "Was not able to restart application, PM null");
                }
            } else {
                Log.e("RAPP", "Was not able to restart application, Context null");
            }
        } catch (Exception ex) {
            Log.e("RAPP", "Was not able to restart application");
        }
    }

    public static void vibrate(Context context, long millis) {
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(millis, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(millis);
        }
    }

    public static void openDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading . . . ");
        progressDialog.show();
    }

    public static void openDialogCanceless(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading . . . ");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public static void closeDialog() {
        progressDialog.dismiss();
    }

    public static String decodeJsonBytes(byte[] json) {
        return new String(json, StandardCharsets.UTF_8);
    }

    public static String formatLongTimestamp(Long timestamp) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(timestamp * 1000L);
        return formatter.format(cal);
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    public static void showSettingsDialog(Activity c) {
        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle("Izin diperlukan");
        builder.setMessage("Aplikasi ini membutuhkan izin kamera untuk dapat memindai QR presensi.");
        builder.setPositiveButton("Masuk ke Pengaturan", (dialog, which) -> {
            dialog.cancel();
            openSettings(c);
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        builder.show();

    }

    // navigating user to app settings
    public static void openSettings(Activity c) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", c.getPackageName(), null);
        intent.setData(uri);
        c.startActivityForResult(intent, Constants.CODE_CAMERA_PERMISSION);
    }

    public static String capitalizeFirstLetter(String original) {
        return (original == null || original.length() == 0)
                ? original
                : original.substring(0, 1).toUpperCase() + original.substring(1);
    }

}
