package uinbandung.kalam.ui.activities.salam;

import android.os.Bundle;

import uinbandung.kalam.R;
import uinbandung.kalam.data.Access;
import uinbandung.kalam.session.User;
import uinbandung.kalam.session.UserSession;
import uinbandung.kalam.ui.activities.base.BaseActivity;
import uinbandung.kalam.ui.fragments.salam.SalamDosenFragment;
import uinbandung.kalam.ui.fragments.salam.SalamMahasiswaFragment;

public class SalamHomeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salam_home);
        User u = UserSession.getInstance().getCurrentUser();

        header("SALAM");

        switch (u.getUserlevel()) {
            case Access.MAHASISWA:
                loadFragment(R.id.salam_frame, new SalamMahasiswaFragment());
                break;
            case Access.DOSEN:
                loadFragment(R.id.salam_frame, new SalamDosenFragment());
                break;
        }

    }
}
