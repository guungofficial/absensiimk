package uinbandung.kalam.ui.activities.salam.salamDosen.kerja_praktik;

import android.os.Bundle;

import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class DataDosenKerjaPraktikActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_dosen_kerja_praktik);

        header("Data Kerja Praktik");
    }
}
