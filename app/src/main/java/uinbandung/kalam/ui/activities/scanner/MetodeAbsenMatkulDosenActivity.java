package uinbandung.kalam.ui.activities.scanner;

import androidx.appcompat.app.AppCompatActivity;
import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

import android.os.Bundle;

public class MetodeAbsenMatkulDosenActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_metode_absen_matkul_dosen);

        header("Pilih Metode Absen");
    }
}
