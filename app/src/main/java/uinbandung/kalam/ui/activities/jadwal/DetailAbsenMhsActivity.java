package uinbandung.kalam.ui.activities.jadwal;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class DetailAbsenMhsActivity extends BaseActivity {

    @BindView(R.id.txt_nama)
    TextView txtNama;
    @BindView(R.id.txt_nim)
    TextView txtNim;
    @BindView(R.id.txt_status)
    TextView txtStatus;
    @BindView(R.id.mainProfile)
    LinearLayout mainProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_absen_mhs);
        ButterKnife.bind(this);

        header("Detail Absen");

        txtNama.setText(getUser().getFullname());
        txtNim.setText(getUser().getUsername());
    }
}
