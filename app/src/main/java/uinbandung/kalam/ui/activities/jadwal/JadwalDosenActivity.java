package uinbandung.kalam.ui.activities.jadwal;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.shreyaspatil.MaterialDialog.MaterialDialog;

import org.threeten.bp.DayOfWeek;
import org.threeten.bp.LocalTime;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uinbandung.kalam.R;
import uinbandung.kalam.models.salam.MatkulKRS;
import uinbandung.kalam.models.salam.SalamKelas;
import uinbandung.kalam.session.User;
import uinbandung.kalam.session.UserSession;
import uinbandung.kalam.ui.activities.base.BaseActivity;
import uinbandung.kalam.ui.activities.salam.kartu.PilihKelasKRSActivity;
import uinbandung.kalam.ui.adapters.salam.PilihKRSAdapter;

import static uinbandung.kalam.utils.Constants.PILIH_KELAS;

public class JadwalDosenActivity extends BaseActivity {
    Spinner spinner;
    @BindView(R.id.txt_nama)
    TextView txtNama;
    @BindView(R.id.txt_nim)
    TextView txtNim;
    @BindView(R.id.txt_status)
    TextView txtStatus;
    @BindView(R.id.mainProfile)
    LinearLayout mainProfile;
    @BindView(R.id.spinnerPilihMatkulKRS)
    Spinner spinnerPilihMatkulKRS;
    @BindView(R.id.rv_pilih_krs_mhs)
    RecyclerView rvPilihKrsMhs;

    List<MatkulKRS> matkulKRSList;
    PilihKRSAdapter pilihKRSAdapter;

    User userSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_krs);
        ButterKnife.bind(this);

        header("Daftar Matakuliah");

        userSession = UserSession.getInstance().getCurrentUser();

        spinner = spinnerPilihMatkulKRS;

        txtNim.setText(userSession.getUsername());
        txtNama.setText(userSession.getFullname());

        ArrayAdapter<String> adapter = new ArrayAdapter<>(JadwalDosenActivity.this, R.layout.spinner_item, getResources().getStringArray(R.array.list_semesterGanjil));
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    case 0:
                        break;
                    case 1:
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        PilihKRSAdapter.KRSMatkulClick mListener = (vh, item, pos) -> {
            Bundle b = new Bundle();
            b.putInt("kelas", pos);
            b.putParcelableArrayList("items", item.getDaftarKelas());

            launchActivityWithResult(JadwalDosenActivity.this, PilihKelasKRSActivity.class, b, PILIH_KELAS);
        };

        PilihKRSAdapter.KRSMatkulClick mCancel = (vh, item, pos) -> {
            new MaterialDialog.Builder(JadwalDosenActivity.this)
                    .setTitle("Anda yakin?")
                    .setMessage("Anda akan membatalkan kelas untuk mata kuliah ini.")
                    .setAnimation(R.raw.warning_rename_animation)
                    .setPositiveButton("Ya", (dialogInterface, which) -> {
                        matkulKRSList.get(pos).setKelasDipilih(-1);
                        pilihKRSAdapter.notifyDataSetChanged();
                        dialogInterface.dismiss();
                    })
                    .setNegativeButton("Tidak", (dialogInterface, which) -> {
                        //
                        //
                        dialogInterface.dismiss();
                    })
                    .setCancelable(false)
                    .build().show();
        };

        matkulKRSList = new ArrayList<>();
        pilihKRSAdapter = new PilihKRSAdapter(matkulKRSList, mListener, mCancel);

        rvPilihKrsMhs.setLayoutManager(new LinearLayoutManager(this));
        rvPilihKrsMhs.setAdapter(pilihKRSAdapter);

        initMatkul();
    }

    private void initMatkul() {
        SalamKelas s = new SalamKelas("B", "Ana", "Ani", DayOfWeek.SUNDAY, LocalTime.of(7, 0), LocalTime.of(8, 40));
        ArrayList<SalamKelas> salamKelasList = new ArrayList<>();

        salamKelasList.add(s);

        matkulKRSList.add(new MatkulKRS("IF14155", "Algoritma Digital", 2, null, salamKelasList));
        matkulKRSList.add(new MatkulKRS("MAT14155", "Kalkulus III", 1, null, salamKelasList));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == BaseActivity.RESULT_OK) {
            switch (requestCode) {
                case PILIH_KELAS:
                    Toast.makeText(JadwalDosenActivity.this, "Sukses", Toast.LENGTH_SHORT).show();
                    int kelas = data.getIntExtra("kelas", -1);
                    int pos = data.getIntExtra("pos", -1);

                    matkulKRSList.get(kelas).setKelasDipilih(pos);
                    pilihKRSAdapter.notifyDataSetChanged();
                default:
                    return;
            }
        }
    }
}
