package uinbandung.kalam.ui.activities.salam.salamDosen;

import android.os.Bundle;

import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class DosenKerjaPraktikActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_kerja_praktik);

        header("Kerja Praktik");
    }
}
