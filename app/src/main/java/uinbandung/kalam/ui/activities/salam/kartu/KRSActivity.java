package uinbandung.kalam.ui.activities.salam.kartu;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TableRow;

import androidx.cardview.widget.CardView;

import butterknife.BindView;
import butterknife.ButterKnife;
import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class KRSActivity extends BaseActivity {


    @BindView(R.id.rowlay)
    TableRow rowlay;
    @BindView(R.id.detailKeterangan)
    CardView detailKeterangan;
    @BindView(R.id.btn_hapusMatkul)
    Button btnHapusMatkul;
    @BindView(R.id.btn_exportKRS)
    Button btnExportKRS;
    @BindView(R.id.btn_tambahMatkul)
    Button btnTambahMatkul;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_krs);
        ButterKnife.bind(this);

        header("Kartu Rencana Studi");

        btnTambahMatkul.setOnClickListener(v -> {
            launchActivity(KRSActivity.this, PilihKRSActivity.class);
        });

    }


}
