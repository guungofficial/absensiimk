package uinbandung.kalam.ui.activities.scanner;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class QRScanResultActivity extends BaseActivity {

    @BindView(R.id.qr_matkul)
    TextView qrMatkul;
    @BindView(R.id.qr_kelas)
    TextView qrKelas;
    @BindView(R.id.qr_jadwal)
    TextView qrJadwal;
    @BindView(R.id.qr_dosen)
    TextView qrDosen;
    @BindView(R.id.matkul_caption)
    TextView matkulCaption;
    @BindView(R.id.verif_matkul)
    Button verifMatkul;
    @BindView(R.id.tutup)
    Button tutup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrscan_result);
        ButterKnife.bind(this);

        setBackButton();
        setTitle("Presensi");

        Bundle b = getIntent().getExtras();

        if (b != null) {
            String matkul = b.getString("matkul");
            String kelas = b.getString("kelas");
            String dosen = b.getString("dosen");
            long jadwal = b.getLong("jadwal");

            qrMatkul.setText(matkul);
            qrKelas.setText(kelas);
            qrDosen.setText(dosen);
            //qrJadwal.setText(CommonUtils.formatLongTimestamp(jadwal));

            verifMatkul.setOnClickListener(v -> verifikasiHadir());
        } else finish();
    }

    private void verifikasiHadir() {
        verifMatkul.setVisibility(View.GONE);
        tutup.setVisibility(View.VISIBLE);
        matkulCaption.setText("Verifikasi Berhasil");

        tutup.setOnClickListener(v -> finish());
    }
}
