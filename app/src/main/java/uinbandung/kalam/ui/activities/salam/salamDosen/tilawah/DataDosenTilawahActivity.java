package uinbandung.kalam.ui.activities.salam.salamDosen.tilawah;

import android.os.Bundle;

import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class DataDosenTilawahActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_dosen_tilawah);

        header("Data Tilawah");
    }
}
