package uinbandung.kalam.ui.activities.scanner.metode_absen_dosen;

import androidx.appcompat.app.AppCompatActivity;
import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

import android.os.Bundle;

public class MatkulDosenQrActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_dosen_qr);

        header("QR Code");
    }
}
