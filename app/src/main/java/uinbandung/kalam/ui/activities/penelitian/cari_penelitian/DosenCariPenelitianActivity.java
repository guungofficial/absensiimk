package uinbandung.kalam.ui.activities.penelitian.cari_penelitian;

import android.os.Bundle;

import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class DosenCariPenelitianActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_cari_penelitian);

        header("Cari Penelitian");
    }
}
