package uinbandung.kalam.ui.activities.salam.survey;

import android.os.Bundle;

import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class SurveyHomeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_home);
    }
}
