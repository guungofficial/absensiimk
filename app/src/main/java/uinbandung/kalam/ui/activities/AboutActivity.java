package uinbandung.kalam.ui.activities;

import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class AboutActivity extends BaseActivity {

    @BindView(R.id.splash_logo)
    ImageView splashLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        ButterKnife.bind(this);

        header("Tentang Aplikasi Ini");

        Glide.with(AboutActivity.this)
                .load(R.drawable.logo)
                .apply(new RequestOptions().override(300))
                .into(splashLogo);
    }
}
