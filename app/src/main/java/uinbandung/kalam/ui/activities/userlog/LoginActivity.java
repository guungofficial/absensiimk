package uinbandung.kalam.ui.activities.userlog;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import uinbandung.kalam.R;
import uinbandung.kalam.data.Access;
import uinbandung.kalam.session.UserSession;
import uinbandung.kalam.ui.activities.MainActivity;
import uinbandung.kalam.ui.activities.base.BaseBangetActivity;

public class LoginActivity extends BaseBangetActivity {

    @BindView(R.id.input_ni)
    EditText inputNi;
    @BindView(R.id.input_password)
    EditText inputPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.my_logo)
    ImageView myLogo;
    @BindView(R.id.gmbr_nim)
    ImageView gmbrNim;
    @BindView(R.id.gmbr_psw)
    ImageView gmbrPsw;
    @BindView(R.id.cek_remember)
    CheckBox cekRemember;
    @BindView(R.id.txt_rmb)
    TextView txtRmb;
    @BindView(R.id.lupa_password)
    TextView lupaPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        btnLogin.setOnClickListener(view -> login());

        Glide.with(this)
                .load(R.drawable.logo)
                .apply(new RequestOptions().override(300))
                .into(myLogo);

        lupaPassword.setOnClickListener(v -> lupaPassword());
    }

    private void login() {
        String[] mhs = {"987654321", "mhs", "Deden Rozzy"};
        String[] dos = {"123456789", "dosen", "Ali Syamsul"};

        if (inputNi.getText().toString().equals("") && inputPassword.getText().toString().equals("")) {
            Toast.makeText(this, "Masukan tidak boleh kosong.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (inputNi.getText().toString().equals(mhs[0]) && inputPassword.getText().toString().equals(mhs[1])) {
            UserSession.getInstance().setCurrentUser(mhs[2], mhs[0], Access.MAHASISWA);
            Toast.makeText(this, "Masuk sebagai mahasiswa.", Toast.LENGTH_SHORT).show();
        } else if (inputNi.getText().toString().equals(dos[0]) && inputPassword.getText().toString().equals(dos[1])) {
            UserSession.getInstance().setCurrentUser(dos[2], dos[0], Access.DOSEN);
            Toast.makeText(this, "Masuk sebagai dosen.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "NIP/NIK atau kata sandi salah.", Toast.LENGTH_SHORT).show();
            return;
        }

        launchActivity(LoginActivity.this, MainActivity.class);
        finish();
    }

    private void lupaPassword() {
        Intent intent;
        intent = new Intent(this, LupaPasswordActivity.class);
        startActivity(intent);
    }
}
