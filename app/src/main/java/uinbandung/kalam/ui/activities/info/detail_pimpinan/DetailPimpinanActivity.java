package uinbandung.kalam.ui.activities.info.detail_pimpinan;

import android.os.Bundle;

import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class DetailPimpinanActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pimpinan);

        header("Detail Pimpinan");
    }
}
