package uinbandung.kalam.ui.activities.salam.salamDosen;

import android.os.Bundle;

import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class DosenBimbinganAkademikActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_bimbingan_akademik);

        header("Bimbingan Akademik");
    }
}
