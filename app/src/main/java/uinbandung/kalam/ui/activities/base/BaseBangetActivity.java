package uinbandung.kalam.ui.activities.base;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import uinbandung.kalam.ui.activities.MainActivity;

public class BaseBangetActivity extends AppCompatActivity {

    public static void setSystemBarTheme(final Activity pActivity, final boolean pIsDark) {
        // Fetch the current flags.
        final int lFlags = pActivity.getWindow().getDecorView().getSystemUiVisibility();
        // Update the SystemUiVisibility dependening on whether we want a Light or Dark theme.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            pActivity.getWindow().getDecorView().setSystemUiVisibility(pIsDark ? (lFlags & ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR) : (lFlags | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR));
        }
    }

    protected final void changeStatusBarColor(Activity activity, int color) {
        Window window = activity.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(activity, color));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            if (getSupportActionBar() != null) {
                getSupportActionBar().setElevation(0f);
            } else if (getActionBar() != null) {
                getActionBar().setElevation(0f);
            }
        } catch (IllegalStateException e) {

        }
    }

    protected void launchActivity(Context c, Class<?> cls, Bundle bundle) {
        Intent i = new Intent(c, cls);
        if (bundle != null) i.putExtras(bundle);
        c.startActivity(i);
    }

    protected void launchActivity(Context c, Class<?> cls) {
        launchActivity(c, cls, null);
    }

    protected void launchActivityWithResult(AppCompatActivity c, Class<?> cls, Bundle bundle, int launchCode) {
        Intent i = new Intent(c, cls);
        if (bundle != null) i.putExtras(bundle);
        c.startActivityForResult(i, launchCode);
    }


    protected void setBackButton() {
        if (getActionBar() != null) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        } else if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void setTranslucentWindows() {
        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
    }

    protected final void gotoMain(Context c) {
        Bundle b = new Bundle();
        b.putBoolean("do_restart", true);

        launchActivity(c, MainActivity.class, b);
        finish();
    }

    protected void header(String text) {
        setBackButton();
        setTitle(text);
    }

    protected void header(String text, String sub) {
        header(text);

        if (getActionBar() != null) {
            getActionBar().setSubtitle(sub);
        } else if (getSupportActionBar() != null) {
            getSupportActionBar().setSubtitle(sub);
        }
    }

    protected void loadFragment(int Viewid, Fragment fragment) {
        getSupportFragmentManager().popBackStack();
        getSupportFragmentManager().beginTransaction()
                .replace(Viewid, fragment)
                .disallowAddToBackStack()
                .commit();
    }
}
