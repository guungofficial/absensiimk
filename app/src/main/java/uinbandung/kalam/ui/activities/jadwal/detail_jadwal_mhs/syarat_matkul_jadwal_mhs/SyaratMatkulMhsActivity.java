package uinbandung.kalam.ui.activities.jadwal.detail_jadwal_mhs.syarat_matkul_jadwal_mhs;

import android.os.Bundle;

import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class SyaratMatkulMhsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_syarat_matkul_mhs);

        header("Syarat Mata Kuliah");
    }
}
