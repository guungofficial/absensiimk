package uinbandung.kalam.ui.activities.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.time.LocalTime;

import uinbandung.kalam.session.User;
import uinbandung.kalam.session.UserSession;
import uinbandung.kalam.ui.activities.userlog.LoginActivity;

public class BaseActivity extends BaseBangetActivity {
    private User user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        user = UserSession.getInstance().getCurrentUser();

        if (user == null) {
            Toast.makeText(this, "Anda belum masuk.", Toast.LENGTH_SHORT).show();
            launchActivity(getApplicationContext(), LoginActivity.class);
            finish();
        }
    }

    public User getUser() {
        return user;
    }

    protected void logout(Context c) {
        UserSession.getInstance().logout();
        Intent intent = new Intent(c, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        c.startActivity(intent);
        finish();
    }
}
