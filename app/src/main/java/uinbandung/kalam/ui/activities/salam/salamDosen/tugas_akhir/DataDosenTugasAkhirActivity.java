package uinbandung.kalam.ui.activities.salam.salamDosen.tugas_akhir;

import android.os.Bundle;

import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class DataDosenTugasAkhirActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_dosen_tugas_akhir);

        header("Data Tugas Akhir");
    }
}
