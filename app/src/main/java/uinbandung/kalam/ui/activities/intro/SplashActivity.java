package uinbandung.kalam.ui.activities.intro;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import uinbandung.kalam.R;
import uinbandung.kalam.session.UserSession;
import uinbandung.kalam.ui.activities.MainActivity;
import uinbandung.kalam.ui.activities.userlog.LoginActivity;

public class SplashActivity extends AppCompatActivity {

    @BindView(R.id.splash_logo)
    ImageView splashLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        Window w = getWindow();
        w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        Glide.with(SplashActivity.this)
                .load(R.drawable.logo)
                .apply(new RequestOptions().override(300))
                .into(splashLogo);

        new Handler().postDelayed(() -> {
            //The following code will execute after the 5 seconds.
            try {
                //Go to next page i.e, start the next activity.
                initlogin();

                //Let's Finish Splash Activity since we don't want to show this when user press back button.
                finish();
            } catch (Exception ignored) {
                ignored.printStackTrace();
            }
        }, 3000);  // Give a 3 seconds delay.
    }

    private void initlogin() {
        if (UserSession.getInstance().getCurrentUser() == null) {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
        }
    }

}
