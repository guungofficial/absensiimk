package uinbandung.kalam.ui.activities.kalender;

import android.os.Bundle;
import android.widget.TextView;

import com.events.calendar.views.EventsCalendar;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uinbandung.kalam.R;
import uinbandung.kalam.models.kalender.DateMark;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class KalenderAkademikActivity extends BaseActivity implements EventsCalendar.Callback{

    @BindView(R.id.kalender_semester)
    TextView kalenderSemester;
    @BindView(R.id.eventsCalendar)
    EventsCalendar eventsCalendar;

    List<DateMark> dateMarkList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kalender_akademik);
        ButterKnife.bind(this);

        header("Kalender Akademik", "Ganjil 2019/2020");

        dateMarkList = new ArrayList<>();

        Calendar today = Calendar.getInstance();
        Calendar end   = Calendar.getInstance();

        end.add(Calendar.YEAR, 2);

        eventsCalendar
                .setToday(Calendar.getInstance())
                .setMonthRange(today, end)
                .setCallback(this)
                .build();

    }

    @Override
    public void onDayLongPressed(@Nullable Calendar calendar) {

    }

    @Override
    public void onDaySelected(@Nullable Calendar calendar) {

    }

    @Override
    public void onMonthChanged(@Nullable Calendar calendar) {

    }
}
