package uinbandung.kalam.ui.activities.salam.salamDosen.tahfidz;

import android.os.Bundle;

import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class DataDosenTahfidzActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_dosen_tahfidz);

        header("Data Tahfidz");
    }
}
