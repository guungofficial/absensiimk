package uinbandung.kalam.ui.activities.jadwal.detail_jadwal_mhs;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;
import uinbandung.kalam.ui.activities.jadwal.detail_jadwal_mhs.syarat_matkul_jadwal_mhs.SyaratMatkulMhsActivity;

public class DetailJadwalMhsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_jadwal_mhs);

        header("Detail Jadwal Mahasiswa");
    }

    public void lihatSyarat(View view) {
        Intent intent = new Intent(DetailJadwalMhsActivity.this, SyaratMatkulMhsActivity.class);
        startActivity(intent);
    }


}
