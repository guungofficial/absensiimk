package uinbandung.kalam.ui.activities.info;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uinbandung.kalam.R;
import uinbandung.kalam.models.info.Pimpinan;
import uinbandung.kalam.ui.activities.base.BaseActivity;
import uinbandung.kalam.ui.adapters.info.InfoPimpinanAdapter;

public class InfoPimpinanActivity extends BaseActivity {
    @BindView(R.id.rv_info_pimpinan)
    RecyclerView rvInfoPimpinan;

    InfoPimpinanAdapter infoPimpinanAdapter;
    List<Pimpinan> pimpinanList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_pimpinan);
        ButterKnife.bind(this);

        header("Info Pimpinan");

        pimpinanList = new ArrayList<>();
        infoPimpinanAdapter = new InfoPimpinanAdapter(pimpinanList);

        rvInfoPimpinan.setLayoutManager(new LinearLayoutManager(this));
        rvInfoPimpinan.setAdapter(infoPimpinanAdapter);

        initInfoPimpinan();
    }

    void initInfoPimpinan() {
        pimpinanList.add(new Pimpinan("Anwar Said", "171621"));
        pimpinanList.add(new Pimpinan("Henry Fuad", "171921"));
    }
}
