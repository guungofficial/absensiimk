package uinbandung.kalam.ui.activities.beasiswa;

import android.os.Bundle;

import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class InfoBeasiswaHomeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_beasiswa_home);

        header("Info Beasiswa");
    }
}
