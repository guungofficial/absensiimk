package uinbandung.kalam.ui.activities.salam.salamDosen.bimbingan_akademik;

import android.os.Bundle;

import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class DataDosenBimbinganAkademikActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_dosen_bimbingan_akademik);

        header("Data Bimbingan Akademik");
    }
}
