package uinbandung.kalam.ui.layoutmanager;

import android.content.Context;
import android.util.AttributeSet;

import androidx.recyclerview.widget.GridLayoutManager;

public class HomeMenuLayoutManager extends GridLayoutManager {
    public HomeMenuLayoutManager(Context context, int cols) {
        super(context, cols);
    }

    @Override
    public boolean canScrollHorizontally() {
        return false;
    }

    @Override
    public boolean canScrollVertically() {
        return false;
    }
}
