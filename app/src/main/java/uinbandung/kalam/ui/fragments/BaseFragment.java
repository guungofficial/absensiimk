package uinbandung.kalam.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public class BaseFragment extends Fragment {
    Unbinder unbinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void bindView(Object target, View source) {
        unbinder = ButterKnife.bind(target, source);
    }

    protected void launchActivity(Context c, Class<?> cls, Bundle bundle) {
        Intent i = new Intent(c, cls);
        if (bundle != null) i.putExtras(bundle);
        c.startActivity(i);
    }

    @Override
    public void onDetach() {
        if (unbinder != null) unbinder.unbind();
        super.onDetach();
    }

    protected void launchActivity(Context c, Class<?> cls) {
        launchActivity(c, cls, null);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
