package uinbandung.kalam.ui.fragments;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import uinbandung.kalam.R;
import uinbandung.kalam.ui.activities.base.BaseActivity;

public class EditProfilFragment extends BaseActivity {

    @BindView(R.id.image_profile)
    CircleImageView imageProfile;
    @BindView(R.id.my_logo)
    ImageView myLogo;
    @BindView(R.id.btn_simpan_data)
    Button btnSimpanData;
    @BindView(R.id.mainEditProfile)
    LinearLayout mainEditProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profil_fragment);
        ButterKnife.bind(this);

        header("Edit Profil");

        btnSimpanData.setOnClickListener(view -> {
            launchActivity(EditProfilFragment.this, HomeFragment.class);
        });

    }
}
