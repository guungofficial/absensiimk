package uinbandung.kalam.ui.fragments.presensi;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import uinbandung.kalam.R;
import uinbandung.kalam.data.Access;
import uinbandung.kalam.session.User;
import uinbandung.kalam.session.UserSession;

/**
 * A simple {@link Fragment} subclass.
 */
public class PresensiFragment extends Fragment {


    public PresensiFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_presensi, container, false);

    }




}
