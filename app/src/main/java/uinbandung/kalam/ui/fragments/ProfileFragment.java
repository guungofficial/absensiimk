package uinbandung.kalam.ui.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import uinbandung.kalam.R;
import uinbandung.kalam.session.User;
import uinbandung.kalam.session.UserSession;
import uinbandung.kalam.ui.activities.userlog.LoginActivity;
import uinbandung.kalam.utils.CommonUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends BaseFragment {



    @BindView(R.id.image_profile)
    CircleImageView imageProfile;
    @BindView(R.id.txt_nama)
    TextView txtNama;
    @BindView(R.id.txt_nim)
    TextView txtNim;
    @BindView(R.id.txt_status)
    TextView txtStatus;
    @BindView(R.id.btn_logout)
    Button btnLogout;
    @BindView(R.id.mainProfile)
    LinearLayout mainProfile;
    @BindView(R.id.btn_edit)
    ImageButton btnEdit;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        User user = UserSession.getInstance().getCurrentUser();

        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        bindView(this, view);

        btnLogout.setOnClickListener(v -> logout(view));

        btnLogout.setVisibility(View.GONE);

        String input = user.getUserlevel();

        txtNama.setText(user.getFullname());
        txtNim.setText(user.getUsername());
        txtStatus.setText(CommonUtils.capitalizeFirstLetter(input));

        btnEdit.setOnClickListener(view1 -> {
            launchActivity(view.getContext(), EditProfilFragment.class);
        });


        return view;
    }

    private void logout(View view) {
        UserSession.getInstance().logout();
        launchActivity(view.getContext(), LoginActivity.class);
        getActivity().finish();
    }
}
