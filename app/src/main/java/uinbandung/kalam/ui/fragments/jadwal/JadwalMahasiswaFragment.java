package uinbandung.kalam.ui.fragments.jadwal;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import uinbandung.kalam.R;
import uinbandung.kalam.models.jadwal.MatakuliahMahasiswa;
import uinbandung.kalam.ui.activities.jadwal.detail_jadwal_mhs.DetailJadwalMhsActivity;
import uinbandung.kalam.ui.adapters.jadwal.JadwalMahasiswaAdapter;
import uinbandung.kalam.ui.fragments.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class JadwalMahasiswaFragment extends BaseFragment {

    List<MatakuliahMahasiswa> moreMenuList;
    JadwalMahasiswaAdapter mahasiswaAdapter;

    @BindView(R.id.jadwal_mhs_rv)
    RecyclerView jadwalMhsRv;

    public JadwalMahasiswaFragment() {
        // Required empty public constructor
        moreMenuList = new ArrayList<>();
        mahasiswaAdapter = new JadwalMahasiswaAdapter(moreMenuList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_jadwal_mahasiswa, container, false);
        bindView(this, v);

        jadwalMhsRv.setLayoutManager(new LinearLayoutManager(container.getContext()));
        jadwalMhsRv.setItemAnimator(new DefaultItemAnimator());
        jadwalMhsRv.setAdapter(mahasiswaAdapter);

        View.OnClickListener ocl = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchActivity(v.getContext(), DetailJadwalMhsActivity.class);
            }
        };

        moreMenuList.add(new MatakuliahMahasiswa("IF1562", "Senin 07:00-08:40", "Kalkulus I", ocl));
        moreMenuList.add(new MatakuliahMahasiswa("IF1563", "Senin 07:00-08:40", "Kalkulus I", ocl));
        moreMenuList.add(new MatakuliahMahasiswa("IF1564", "Senin 07:00-08:40", "Kalkulus I", ocl));

        return v;
    }

}