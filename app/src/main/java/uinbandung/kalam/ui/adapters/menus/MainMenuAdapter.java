package uinbandung.kalam.ui.adapters.menus;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uinbandung.kalam.R;
import uinbandung.kalam.models.menus.MainMenu;

public class MainMenuAdapter extends RecyclerView.Adapter<MainMenuAdapter.MyViewHolder> {

    List<MainMenu> moreMenuList;

    public MainMenuAdapter(List<MainMenu> moreMenuList) {
        this.moreMenuList = moreMenuList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_menu, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        MainMenu item = moreMenuList.get(position);

        holder.mainMenuCaption.setText(item.getName());

        Glide.with(holder.itemView.getContext())
                .load(item.getIcon())
                .into(holder.mainMenuImg);

        //holder.mainMenuImg.setColorFilter(item.getTint());

        if (item.getOnClick() != null) holder.mainMenuLayout.setOnClickListener(item.getOnClick());
    }

    @Override
    public int getItemCount() {
        return moreMenuList.size();
    }

    public void add(MainMenu i) {
        moreMenuList.add(i);
        notifyDataSetChanged();
        notify();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.main_menu_img)
        ImageView mainMenuImg;
        @BindView(R.id.main_menu_caption)
        TextView mainMenuCaption;
        @BindView(R.id.main_menu_layout)
        LinearLayout mainMenuLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
