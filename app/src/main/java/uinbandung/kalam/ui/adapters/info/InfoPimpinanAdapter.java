package uinbandung.kalam.ui.adapters.info;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uinbandung.kalam.R;
import uinbandung.kalam.models.info.Pimpinan;
import uinbandung.kalam.ui.activities.info.detail_dosen.DetailDosenActivity;

public class InfoPimpinanAdapter extends RecyclerView.Adapter<InfoPimpinanAdapter.MyViewHolder> {
    private List<Pimpinan> pimpinanList = new ArrayList<>();

    public InfoPimpinanAdapter(List<Pimpinan> pimpinanList) {
        this.pimpinanList = pimpinanList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_info_person, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Pimpinan item = pimpinanList.get(position);

        holder.niPerson.setText(item.getNip());
        holder.namaPerson.setText(item.getName());

        holder.layoutInfoPerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(holder.itemView.getContext(), DetailDosenActivity.class);
                i.putExtra("nip", item.getNip());
                holder.itemView.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return pimpinanList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.person_dp)
        ImageView personDp;
        @BindView(R.id.nama_person)
        TextView namaPerson;
        @BindView(R.id.ni_person)
        TextView niPerson;
        @BindView(R.id.pilihan_kelas)
        RelativeLayout pilihanKelas;
        @BindView(R.id.layout_info_person)
        LinearLayout layoutInfoPerson;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
