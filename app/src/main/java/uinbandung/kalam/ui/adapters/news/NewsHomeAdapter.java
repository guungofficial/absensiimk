package uinbandung.kalam.ui.adapters.news;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uinbandung.kalam.R;
import uinbandung.kalam.models.news.NewsHome;

public class NewsHomeAdapter extends RecyclerView.Adapter<NewsHomeAdapter.MyViewHolder> {

    private List<NewsHome> list;

    public NewsHomeAdapter(List<NewsHome> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        NewsHome item = list.get(position);

        holder.newsTitle.setText(item.getTitle());
        holder.newsDesc.setText(item.getSubtitle());

        Glide.with(holder.itemView.getContext()).load(item.getPic())
                .into(holder.newsThumb);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.news_title)
        TextView newsTitle;
        @BindView(R.id.news_desc)
        TextView newsDesc;
        @BindView(R.id.news_thumb)
        ImageView newsThumb;
        @BindView(R.id.layout_news)
        RelativeLayout layoutNews;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
