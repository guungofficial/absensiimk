package uinbandung.kalam.ui.adapters.jadwal;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uinbandung.kalam.R;
import uinbandung.kalam.models.jadwal.MatakuliahMahasiswa;

public class JadwalMahasiswaAdapter extends RecyclerView.Adapter<JadwalMahasiswaAdapter.MyViewHolder> {
    private List<MatakuliahMahasiswa> matkul;


    public JadwalMahasiswaAdapter(List<MatakuliahMahasiswa> matkul) {
        this.matkul = matkul;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_jadwal, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        MatakuliahMahasiswa item = matkul.get(position);

        holder.jadwalKodeMatkul.setText(item.getKodeMk());
        holder.jadwalMatkul.setText(item.getNama());
        holder.jadwalTanggal.setText(item.getJadwal());

        if (item.getOcl() != null) holder.jadwalLayout.setOnClickListener(item.getOcl());
    }

    @Override
    public int getItemCount() {
        return matkul.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.jadwal_kode_matkul)
        TextView jadwalKodeMatkul;
        @BindView(R.id.jadwal_tanggal)
        TextView jadwalTanggal;
        @BindView(R.id.jadwal_matkul)
        TextView jadwalMatkul;
        @BindView(R.id.jadwal_layout)
        LinearLayout jadwalLayout;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
