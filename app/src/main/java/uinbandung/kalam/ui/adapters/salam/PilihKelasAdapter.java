package uinbandung.kalam.ui.adapters.salam;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import uinbandung.kalam.R;
import uinbandung.kalam.models.salam.SalamKelas;
import uinbandung.kalam.utils.DateTimeUtils;

public class PilihKelasAdapter extends RecyclerView.Adapter<PilihKelasAdapter.MyViewHolder> {
    private List<SalamKelas> list;
    private KelasClick mClick;

    public PilihKelasAdapter(List<SalamKelas> list, KelasClick mClick) {
        this.list = list;
        this.mClick = mClick;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pilih_krs_kelas, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        SalamKelas item = list.get(position);


        String jadwal = String.format(Locale.US, "%s (%s-%s)",
                DateTimeUtils.getIndonesianDay(item.getHari()),
                DateTimeUtils.parseTime(item.getJam_masuk()),
                DateTimeUtils.parseTime(item.getJam_selesai())
        );

        holder.jadwalKelas.setText(jadwal);
        holder.indikatorKelas.setText(item.getNama_kelas());
        holder.pilihKrsDosen1.setText(item.getDosen1());
        holder.pilihKrsDosen2.setText(item.getDosen2());

        holder.layoutPilihKrsKelas.setOnClickListener(v -> mClick.onClick(holder, item, position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface KelasClick {
        void onClick(MyViewHolder vh, SalamKelas item, int pos);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.indikator_kelas)
        TextView indikatorKelas;
        @BindView(R.id.jadwal_kelas)
        TextView jadwalKelas;
        @BindView(R.id.pilih_krs_dosen_1)
        TextView pilihKrsDosen1;
        @BindView(R.id.pilih_krs_dosen_2)
        TextView pilihKrsDosen2;
        @BindView(R.id.layout_pilih_krs_kelas)
        LinearLayout layoutPilihKrsKelas;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
