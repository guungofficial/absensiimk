package uinbandung.kalam.models.jadwal;

import android.view.View;

public class MatakuliahMahasiswa extends Matakuliah {
    View.OnClickListener ocl;

    public MatakuliahMahasiswa(String kodeMk, String jadwal, String nama, View.OnClickListener ocl) {
        super(kodeMk, jadwal, nama);
        this.ocl = ocl;
    }

    public View.OnClickListener getOcl() {
        return ocl;
    }

    public void setOcl(View.OnClickListener ocl) {
        this.ocl = ocl;
    }
}
