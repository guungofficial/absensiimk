package uinbandung.kalam.models.info;

public class Pimpinan extends Person {
    private String nip;

    public Pimpinan(String name, String nip) {
        super(name);
        this.nip = nip;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }
}
