package uinbandung.kalam.models.salam;

import android.util.Pair;

import java.util.ArrayList;

public class SalamMatakuliah {
    private String kodeMk, nama;
    private int semester;
    private ArrayList<Pair<SalamMatakuliah, String>> prasyarat;

    public SalamMatakuliah(String kodeMk, String nama, int semester, ArrayList<Pair<SalamMatakuliah, String>> prasyarat) {
        this.kodeMk = kodeMk;
        this.nama = nama;
        this.semester = semester;
        this.prasyarat = prasyarat;
    }

    public String getKodeMk() {
        return kodeMk;
    }

    public void setKodeMk(String kodeMk) {
        this.kodeMk = kodeMk;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getSemester() {
        return semester;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    public ArrayList<Pair<SalamMatakuliah, String>> getPrasyarat() {
        return prasyarat;
    }

    public void setPrasyarat(ArrayList<Pair<SalamMatakuliah, String>> prasyarat) {
        this.prasyarat = prasyarat;
    }
}
