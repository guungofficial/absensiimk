package uinbandung.kalam.models.salam;

//Model ini hanya untuk ditampilkan dalam menu pilih KRS dan tidak akan masuk ke API.

import android.util.Pair;

import java.util.ArrayList;

public class MatkulKRS extends SalamMatakuliah {
    private ArrayList<SalamKelas> daftarKelas;
    private int kelasDipilih = -1;

    public MatkulKRS(String kodeMk, String nama, int semester, ArrayList<Pair<SalamMatakuliah, String>> prasyarat, ArrayList<SalamKelas> daftarKelas) {
        super(kodeMk, nama, semester, prasyarat);
        this.daftarKelas = daftarKelas;
    }

    public ArrayList<SalamKelas> getDaftarKelas() {
        return daftarKelas;
    }

    public void setDaftarKelas(ArrayList<SalamKelas> daftarKelas) {
        this.daftarKelas = daftarKelas;
    }

    public int getKelasDipilih() {
        return kelasDipilih;
    }

    public void setKelasDipilih(int kelasDipilih) {
        this.kelasDipilih = kelasDipilih;
    }
}
