package uinbandung.kalam.models.kalender;

import java.util.Calendar;

public class DateMark {
    private String name, desc;
    private Calendar start, finish;

    public DateMark(String name, String desc, Calendar start, Calendar finish) {
        this.name = name;
        this.desc = desc;
        this.start = start;
        this.finish = finish;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Calendar getStart() {
        return start;
    }

    public void setStart(Calendar start) {
        this.start = start;
    }

    public Calendar getFinish() {
        return finish;
    }

    public void setFinish(Calendar finish) {
        this.finish = finish;
    }
}
