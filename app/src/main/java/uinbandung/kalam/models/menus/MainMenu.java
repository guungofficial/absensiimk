package uinbandung.kalam.models.menus;

import android.view.View;

import uinbandung.kalam.R;

public class MainMenu {
    private String name;
    private int icon;
    private View.OnClickListener onClick;
    private int tint;

    public MainMenu(String name, int icon, View.OnClickListener onClick) {
        this(name, icon, onClick, R.color.black);
    }

    public MainMenu(String name, int icon, View.OnClickListener onClick, int tint) {
        this.name = name;
        this.icon = icon;
        this.onClick = onClick;
        this.tint = tint;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public View.OnClickListener getOnClick() {
        return onClick;
    }

    public void setOnClick(View.OnClickListener onClick) {
        this.onClick = onClick;
    }

    public int getTint() {
        return tint;
    }

    public void setTint(int tint) {
        this.tint = tint;
    }
}
