package uinbandung.kalam.models.news;

public class NewsHome {
    private String news_id, title, subtitle, pic;

    public NewsHome(String news_id, String title, String subtitle, String pic) {
        this.news_id = news_id;
        this.title = title;
        this.subtitle = subtitle;
        this.pic = pic;
    }


    public String getNews_id() {
        return news_id;
    }

    public void setNews_id(String news_id) {
        this.news_id = news_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }
}
